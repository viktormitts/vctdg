package vctdg.gui

import scalafx.scene.shape._
import scalafx.scene.paint.Color._
import scalafx.application.Platform
import vctdg._

object EnemyOnMap {
  def apply(enemy: Enemy) = new Rectangle {
    //println("new")
    val i = enemy.game.enemies.last
    //println(i)
    object Move extends Runnable {
      def run = {
        while(i.hp > 0) {
          Platform.runLater {
            x = i.x - 8
            y = i.y - 8
          }
          Thread.sleep(i.pace)
        }
      }
    }
    //x <== i.xprop - 8
    //y <== i.yprop - 8
    width = 16
    height = 16
    enemy.enemy match {
      case StrongEnemy => fill = Red
      case FastEnemy => fill = Purple
    }
    //println("of this")
    new Thread(Move).start
  }
}