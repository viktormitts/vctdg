package vctdg.gui

import scalafx.scene.Node
import scalafx.scene.shape._
import scalafx.scene.paint.Color._
import scalafx.Includes._
import vctdg._

object TowerOnMap {
  def apply(tower: Tower, grid: Short): Node = {
    val thing = graphic(tower.tower, grid)
    thing.onMouseClicked = () => {
      tower.upgrade(tower.tower.upgrade)
    }
    thing
  }
  def graphic(tower: TowerTrait, grid: Short): Node = tower match {
    case GunTower => new Rectangle {
      width = grid
      height = grid
      fill = Gray
    }
    case AreaTower => new Circle {
      radius = grid / 2
      fill = Cyan
    }
  }
}