package vctdg.gui

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.Includes._
import scalafx.scene.{Scene, Node}
import scalafx.scene.paint.Color._
import scalafx.scene.layout._ //{VBox, Pane, StackPane, GridPane, ColumnConstraints, RowConstraints, HBox, Background, BackgroundFill}
import scalafx.geometry.Insets
import scalafx.scene.shape.Rectangle
//import scalafx.scene.control.Button
import scalafx.beans.property._
import scalafx.concurrent.Task
import scalafx.application.Platform
import scalafx.scene.input.KeyEvent
import scalafx.scene.input.KeyCode
import scalafx.scene.text.Text
import scalafx.geometry.Pos._
import javafx.event.EventHandler
import vctdg.Upgrade._
import vctdg._

object GUI extends JFXApp {
  val g = new Game(Map.fromFile("testmap.tdm"))
  val cc = new ColumnConstraints(g.map.grid)
  val rc = new RowConstraints(g.map.grid)
  val towerGrid = new GridPane {
    background = new Background(Array(new BackgroundFill(Green, new CornerRadii(0), Insets(0))))
    columnConstraints = Vector.fill(g.map.x)(cc)
    rowConstraints = Vector.fill(g.map.y)(rc)
    //gridLinesVisible = true
    g.map.path.foreach(x => if (x._1 >= 0 && x._2 >= 0 && x._1 < g.map.x && x._2 < g.map.y) add(new Rectangle {width = g.map.grid; height = g.map.grid; fill = Brown}, x._1, x._2))
    g.towers.foreach(x => add(TowerOnMap(x, g.map.grid), x.x, x.y))
  }
  val money = StringProperty(g.money.toString)
  val placementGrid = new GridPane {
    columnConstraints = Vector.fill(g.map.x)(cc)
    rowConstraints = Vector.fill(g.map.y)(rc)
    gridLinesVisible = true
    val tower: ObjectProperty[TowerTrait] = ObjectProperty(GunTower)
    for {i <- 0 until g.map.x
         j <- 0 until g.map.y} {
      add(new Rectangle {
        onMouseClicked = () => {
          //println("yoya")
          val the = g.buildTower(tower.get, i, j)
          if (the.isDefined) {
            //println("yeya")
            val got = the.get
            towerGrid.add(TowerOnMap(got, g.map.grid), got.x, got.y)
            money.value = g.money.toString
          }
        }
        //println(onMouseClicked)
        width = g.map.grid
        height = g.map.grid
        fill <== when (hover) choose color(0.5, 0.5, 0.5, 0.5) otherwise color(0,0,0,0)
      }, i, j)
    }
  }
  val field = new Pane {
    children = towerGrid
  }
  //println("sizes " + field.getWidth + ", " + field.getHeight)
  field.setPrefSize(g.map.x * g.map.grid, g.map.y * g.map.grid)
  //val enemiesOnMap = scala.collection.mutable.ArrayBuffer[Node]()
  def nextWave = if (!g.spawning) {
    field.children = towerGrid
    g.spawnWave
    object Spawn extends Runnable {
    //val spawn = Task {
      def run = while (g.spawning) { //Platform.runLater {
        //Thread.sleep(1000)
        //println("yenemy " + g.enemies.last)
        //field.children = Seq(towerGrid) ++ enemiesOnMap
        //println(g.spawning)
        Platform.runLater(field.children.addAll(EnemyOnMap(g.enemies.last)))
        //println(field.children)
        Thread.sleep(1000)
      }
    }
    new Thread(Spawn).start
  }
  stage = new PrimaryStage {
    sizeToScene
    title = "Very Cool Tower Defense Game"
    scene = new Scene {
      //fill = Green
      val placing = BooleanProperty(false)
      content = new VBox {
        children = Seq(field, new HBox {
          alignment = CenterLeft
          background = new Background(Array(new BackgroundFill(Black, new CornerRadii(0), Insets(0))))
          def towerButton(tower: TowerTrait): Node = {
            val button = new StackPane {
              children = Seq(new Rectangle {
                fill <== when (placementGrid.tower === tower && placing) choose LightGrey otherwise color(.2,.2,.2)
                val size = g.map.grid + 4
                width = size
                height = size
              }, TowerOnMap.graphic(tower, g.map.grid))
            }
            button.onMouseClicked = () => {
              placementGrid.tower.value = tower
              if (!placing.get) {
                field.children.addAll(placementGrid)
                placing.value = true
              }
            }
            button
          }
          /*object Closer extends EventHandler[KeyEvent] {
            println("it's here")
            def handle(e: KeyEvent) = e.code match {
              case KeyCode.Escape => if (placing) field.children.-=(placementGrid)
              case _ => println("no")
            }
          }*/
          children = Seq(towerButton(GunTower), towerButton(AreaTower), new Text {
            val health = StringProperty(g.health.toString)
            object Update extends Runnable {
              def run = {
                while (g.health > 0) {
                  Platform.runLater{
                    health.value = g.health.toString
                    money.value = g.money.toString
                  }
                  Thread.sleep(100)
                }
                Platform.runLater(health.value = g.health.toString)
                val tt = new Text {
                  text = "Game Over"
                  style = "-fx-font-size: 5em"
                  y = 100
                }
                Platform.runLater(field.children.addAll(tt))
              }
            }
            text <== StringProperty("Money: ") + money + StringProperty(s"\n") + StringProperty("Health: ") + health
            fill = White
            new Thread(Update).start
          }, new StackPane {
            children = Seq(new Rectangle {
              fill = Blue
              width = 100
              height = 32
            }, new Text {
              text = "Next Wave"
            })
            onMouseClicked = () => {
              nextWave
            }
          })
        })
      }
      onKeyPressed = (v: KeyEvent) => v.code match {
        case KeyCode.Escape => if (placing.get) {
          field.children -= placementGrid
          placing.value = false
        }
        case _ =>
      }
    }
  }
}