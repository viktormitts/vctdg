package vctdg

import vctdg.Upgrade._

trait TowerTrait {
  def shoot(target: Enemy, enemies: Seq[Enemy], dmg: Int, tower: Tower)
  val price: Int
  val damage: Int
  val range: Int
  val firerate: Int
  val upgrade: Upgrade
}

object GunTower extends TowerTrait {
  def shoot(target: Enemy, enemies: Seq[Enemy], dmg: Int, tower: Tower) = target.hit(damage)
  val price = 100
  val damage = 10
  val range = 100
  val firerate = 500
  val upgrade = Bomb
}

object AreaTower extends TowerTrait {
  def shoot(target: Enemy, enemies: Seq[Enemy], dmg: Int, tower: Tower) = {
    val me = Map.pixel(tower.x, tower.y, tower.game.map.grid)
    for(i <- enemies) {
      if(Math.pow(i.x-me._1, 2) + Math.pow(i.y-me._2, 2) <= range) {
        i.hit(dmg)
      }
    }
  }
  val price = 50
  val damage = 10
  val range = 48
  val firerate = 1000
  val upgrade = Range
}