package vctdg

import scala.collection.mutable.{ArrayBuffer, Queue}

class Game(val map: Map) {
  println("Initializing game")
  private val _towers = ArrayBuffer[Tower]()
  private var _money = 1000
  private var _health = 100
  
  def health = _health
  def damage(d: Int) = _health -= d
  def earn(m: Int) = _money += m
  def pay(m: Int) = _money -= m
  
  private val waves = Queue[Vector[EnemyTrait]]()
  for (i <- map.enemyWaves) {
    waves.enqueue(i)
  }
  private val currentWave = Queue[EnemyTrait]()
  val enemies = ArrayBuffer[Enemy]()
  private val dis = this
  private var _spawning = false
  def spawning = _spawning
  def spawnWave = if (!_spawning && !enemies.exists(_.hp > 0)) {
    enemies.clear
    val newWave = waves.dequeue
    for (i <- newWave) {
      currentWave.enqueue(i)
    }
    object Spawn extends Runnable {
      def run = {
        _spawning = true
        while (!currentWave.isEmpty) {
          //println("benemy")
          enemies += new Enemy(currentWave.dequeue, dis)
          Thread.sleep(1000)
        }
        _spawning = false
      }
    }
    new Thread(Spawn).start
  }
  
  def towers = _towers.toVector
  def money = _money
  def buildTower(tower: TowerTrait, x: Int, y: Int): Option[Tower] = {
    if (tower.price <= _money && !map.invalid((x, y)) && (towers.isEmpty || !_towers.exists(t => t.x == x && t.y == y))) {
      val newTower = new Tower(tower, x, y, this)
      _towers += newTower
      _money -= tower.price
      Some(newTower)
    }
    else {
      println("Could not build tower")
      None
    }
  }
  
  object Alive extends Runnable {
    def run = {
      while (_health > 0) {
        Thread.sleep(100)
      }
      println("Game Over.")
    }
  }
  new Thread(Alive).start
}