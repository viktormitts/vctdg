package vctdg

class Upgrade(val damage: Int => Int = i => i,
              val range: Int => Int = i => i,
              val firerate: Int => Int = i => i,
              val shoot: Option[(Enemy, Seq[Enemy], Int, Tower) => Unit] = None,
              val price: Int = 25)//,
              //val tower: TowerTrait)
              
object Upgrade {
  val Bomb = new Upgrade(
      damage = d => (d.toDouble * 1.5).toInt,
      shoot = Some((enemy, enemies, dmg, tower) => {
        for(i <- enemies) {
          if(Math.pow(i.x-enemy.x, 2) + Math.pow(i.y-enemy.y, 2) <= 25) {
            i.hit(dmg)
          }
        }
      }))//,
      //tower = GunTower)
  val Range = new Upgrade(
      range = r => r + 32)//,
      //tower = AreaTower)
}