package vctdg

class Enemy(val enemy: EnemyTrait, val game: Game) {
  var x = game.map.startCenter._1
  var y = game.map.startCenter._2
  private var _progress = 0
  def progress = _progress
  private var _hp = enemy.health
  def hp = _hp
  val distance = enemy.distance
  val pace = enemy.pace
  private val dis = this
  def hit(damage: Int) = {
    _hp -= damage
    println(dis + " (" + x + "," + y + ") takes " + damage + " damage and is now at " + _hp)
    if (_hp <= 0 ) {
      println(dis + " died")
    }
  }
  private var direction: Short = game.map.start._3
  def move(direction: Int) = {
    direction match {
      case 0 => y -= distance
      case 1 => x += distance
      case 2 => y += distance
      case 3 => x -= distance
    }
    _progress += 1
  }
  private var going = true
  object Move extends Runnable {
    def run() = {
      var tile = game.map.tile(x, y)
      while(_hp > 0) {
        if(tile != game.map.tile(x, y)) {
          println(dis + " is at (" + x + ", " + y + ")")
          tile = game.map.tile(x, y)
        }
        if(game.map.turnsCenter.contains((x, y))) {
          direction = game.map.turnsCenter((x, y)).direction
          game.map.turnsCenter((x, y)).update
        }
        else if (game.map.endCenter == (x, y)) {
          game.damage(enemy.damage)
          _hp = 0
          going = false
        }
        move(direction)
        Thread.sleep(pace)
      }
      if (going) game.earn(enemy.reward)
    }
  }
  new Thread(Move).start()
}