package vctdg

class InvalidMapFileException(message: String) extends Exception(message)