package vctdg

trait EnemyTrait {
  val health: Int
  val distance: Int
  val pace: Int
  val damage: Int
  val reward: Int
}

object StrongEnemy extends EnemyTrait {
  val health = 100
  val distance = 1
  val pace = 20
  val damage = 5
  val reward = 50
}

object FastEnemy extends EnemyTrait {
  val health = 50
  val distance = 1
  val pace = 10
  val damage = 2
  val reward = 50
}