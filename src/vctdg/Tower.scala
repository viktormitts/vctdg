package vctdg

class Tower(val tower: TowerTrait, val x: Int, val y: Int, val game: Game) {
  private val xc = game.map.center(x)
  private val yc = game.map.center(y)
  private val dis = this
  println(dis + " built at (" + x + ", " + y + ") (" + xc + ", " + yc + ")")
  //val upgrades = scala.collection.mutable.ArrayBuffer[Upgrade]()
  var damage = tower.damage
  var range = tower.range
  var firerate = tower.firerate
  var shoot = tower.shoot _
  def upgrade(upgrade: Upgrade) = if (game.money >= upgrade.price) {
    println("Upgraded " + dis)
    game.pay(upgrade.price)
    damage = upgrade.damage(damage)
    range = upgrade.range(range)
    firerate = upgrade.firerate(firerate)
    shoot = upgrade.shoot.getOrElse(shoot)
  }
  object Main extends Runnable {
    def run = {
      var target: Option[Enemy] = None
      var furthest = 0
      while(true) {
        //println("(" + x + "," + y + ") checks")
        for(i <- game.enemies) {
          //println((i.x - xc) + ", " + (i.y - yc) + ", " + ((i.x - xc)*(i.x - xc) + (i.y - yc)*(i.y - yc)))
          if((i.x - xc)*(i.x - xc) + (i.y - yc)*(i.y - yc) <= range*range && i.progress > furthest && i.hp > 0) {
            target = Some(i)
            furthest = i.progress
          }
        }
        if (target.isDefined) {
          println(dis + " (" + x + "," + y + ") shoots " + target.get + " (" + target.get.x + "," + target.get.y + ")")
          shoot(target.get, game.enemies, damage, dis)
          target = None
          furthest = 0
          Thread.sleep(firerate)
        }
      }
    }
  }
  new Thread(Main).start
}