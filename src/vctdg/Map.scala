package vctdg

//import scala.collection.immutable.HashMap
import scala.collection.mutable.{ArrayBuffer, HashSet, HashMap}
import scala.io.Source

trait Turn {
  def direction: Short
  def update
}

object Up extends Turn {
  def direction = 0
  def update = Unit
}

object Right extends Turn {
  def direction = 1
  def update = Unit
}

object Down extends Turn {
  def direction = 2
  def update = Unit
}

object Left extends Turn {
  def direction = 3
  def update = Unit
}

case class Junction(val directions: Vector[Short]) extends Turn {
  private var _direction = 0
  private val n = directions.length
  def direction = directions(_direction)
  def update = _direction = (_direction + 1) % n
}

class Map(val x: Int, val y: Int, val start: (Int, Int, Short), val end: (Int, Int), val turns: scala.collection.immutable.Map[(Int, Int), Turn], val grid: Short = 32, invalidTowerLocations: Set[(Int, Int)], val enemyWaves: Vector[Vector[EnemyTrait]]) {
  println("End " + end)
  def center(i: Int) = i * grid + grid / 2
  def tile(x: Int, y: Int): (Int, Int) = (x/grid, y/grid)
  val turnsCenter = turns.map(i => ((center(i._1._1), center(i._1._2)), i._2))
  //for(i <- turns) {
  //  turnsCenter += ((center(i.x), center(i.y)) -> i)
  //}
  val startCenter = (center(start._1), center(start._2))
  val endCenter = (center(end._1), center(end._2))
  val path = {
    def walk(start: (Int, Int), directione: (Int, Int)): Set[(Int, Int)] = {
      var current = start
      var direction = directione
      val collect = scala.collection.mutable.HashSet.empty[(Int, Int)]
      while(current != end && !collect(current) && !turns.getOrElse(current, Up).isInstanceOf[Junction]) {
        collect += current
        current = (current._1 + direction._1, current._2 + direction._2)
        println("Current " + current)
        if(turns.contains(current)) {
          turns(current) match {
            case i:Junction => i.directions.foreach(x => collect ++= walk(
                (current._1 + Map.directions(x)._1, current._2 + Map.directions(x)._2), Map.directions(x)))
            case _ => direction = Map.directions(turns(current).direction)
          }
        }
      }
      collect += current
      collect.toSet
    }
    walk((start._1, start._2), Map.directions(start._3))
  }
  val invalid = invalidTowerLocations ++ path
}

object Map {
  val directions = Vector((0, -1), (1, 0), (0, 1), (-1, 0))
  def pixel(x: Int, y: Int, grid: Int): (Int, Int) = (x*grid, y*grid)
  
  def fromFile(name: String) = {
    val source = Source.fromFile(name)
    var i = 'E'
    var x = 1
    var y = 1
    var startx = 0
    var starty = 0
    var startd: Short = 1
    var endx = 0
    var endy = 0
    val turns = HashMap[(Int, Int), Turn]()
    var grid: Short = 32
    val invalid = HashSet.empty[(Int, Int)]
    val waves = ArrayBuffer[ArrayBuffer[EnemyTrait]]()
    def read32 = source.next * 0x10000 + source.next
    def read16 = source.next.toShort
    var head = ""
    for(i <- 1 to 5) {
      println(head)
      head = head + source.next
    }
    if(head != "VCTDG") throw new InvalidMapFileException("Unknown file type")
    try {
      while(source.hasNext) {
        i = source.next
        i match {
          case 'x' => x = read32
          case 'y' => y = read32
          case 's' => {
            startx = read32
            starty = read32
            startd = read16
            if(startd != 0 && startd != 1 && startd != 2 && startd != 3) throw new InvalidMapFileException("Invalid direction " + startd)
          }
          case 'e' => {
            endx = read32
            endy = read32
          }
          case 'u' => turns += (((read32, read32), Up))
          case 'd' => turns += (((read32, read32), Down))
          case 'l' => turns += (((read32, read32), Left))
          case 'r' => turns += (((read32, read32), Right))
          case 'j' => {
            val x = read32
            val y = read32
            val n = read16
            val directions = new ArrayBuffer[Short]
            for(j <- 0 until n) {
              val dir = read16
              if(dir == 0 || dir == 1 || dir == 2 || dir == 3) directions += dir
              else throw new InvalidMapFileException("Invalid direction " + dir)
            }
            turns += (((x, y), new Junction(directions.toVector)))
          }
          case 'g' => grid = read16
          case 'i' => invalid += ((read32, read32))
          case 'w' => {
            val collect = ArrayBuffer[EnemyTrait]()
            for(i <- 0 until read32) {
              source.next match {
                case 's' => collect += StrongEnemy
                case 'f' => collect += FastEnemy
                case e: Char => println("Unknown enemy " + e)
              }
            }
            waves += collect
          }
          case _ =>
        }
      }
    } catch {
      case e:NoSuchElementException => throw new InvalidMapFileException("Unexpected EOF")
    }
    source.close
    new Map(x, y, (startx, starty, startd), (endx, endy), turns.toMap, grid, invalid.toSet, waves.map(_.toVector).toVector)
  }
}